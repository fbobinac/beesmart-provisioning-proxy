<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('cwsNew/{key}/server', [
    'as' => 'custom-soap.server.wsdl',
    'uses' => '\App\Http\Controllers\CustomSoapController@server'
]);

Route::post('cwsNew/{key}/server', [
    'as' => 'custom-soap.server',
    'uses' => '\App\Http\Controllers\CustomSoapController@server'
]);


// app()->router->get('cws/{key}/server', [
//     'as' => 'cws.server.wsdl',
//     'uses' => '\Viewflex\Zoap\ZoapController@server'
// ]);

// app()->router->post('cws/{key}/server', [
//     'as' => 'cws.server',
//     'uses' => '\Viewflex\Zoap\ZoapController@server'
// ]);
