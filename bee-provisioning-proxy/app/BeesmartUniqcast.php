<?php

namespace App;

use App\Providers\UniqcastProvider;

class BeesmartUniqcast
{
    /** * @SuppressWarnings(PHPMD.StaticAccess) */
    public function createUser($userArray)
    {
        // adjust data for uniqcast
        $uniqCastData = array(
            'type' => 'PREPAID',
            'first_name' => $userArray->name,
            'last_name' => $userArray->surname,
            'uid' => $userArray->subscriberUid,
            'external_uid' => null,
            'password' => '123456',
            'region_id' => $userArray->regionUid, // must be number need additional check
            'status' => $userArray->status,
            'operator_id' => 1
        );
        $uniq = new UniqcastProvider;
        return $uniq->createSubscriber($uniqCastData);
    }

    public function updateUser($userUpdateArray)
    {

        $uniqCastData = array(
            'type' => 'PREPAID',
            'first_name' => $userUpdateArray->name,
            'last_name' => $userUpdateArray->surname,
            'uid' => $userUpdateArray->subscriberUid,
            'external_uid' => null,
            'password' => '123456',
            'region_id' => $userUpdateArray->regionUid, // must be number need additional check
            'status' => $userUpdateArray->status,
            'operator_id' => 1
        );

        $queryParams = array('uid' => $userUpdateArray->subscriberUid);
        $uniq = new UniqcastProvider;
        return $uniq->updateSubscriber($uniqCastData, $queryParams);
    }


    public function getUserById($userId)
    {

        $uniqCastData = array(
            'id' => $userId['data']['id']
        );
        $uniq = new UniqcastProvider;
        return $uniq->getSubscriber($uniqCastData);
    }

    public function getSubscriberByUid($subscriberUid)
    {
        $uniqCastData = array(
            'uid' => $subscriberUid
        );
        $uniq = new UniqcastProvider;
        return $uniq->getSubscriber($uniqCastData);
    }

    public function getUserDevice($deviceUid)
    {

        $uniqCastData = array(
            'uid' => $deviceUid
        );
        $uniq = new UniqcastProvider;
        return $uniq->getDevice($uniqCastData);
    }

    public function addUserDevice($deviceUid, $subscriberUid)
    {
        $uniqCastData = array(
            'device_class' => 'STB',
            'operator_id' => 1,
            'uid' => $deviceUid,
            'subscriber_id' => $subscriberUid
        );
        $uniq = new UniqcastProvider;
        $queryParams = array('uid' => $deviceUid);
        return $uniq->connectDevice($uniqCastData, $queryParams);
    }

    public function removeUserDevice($deviceUid)
    {
        $uniqCastData = array(
            'device_class' => 'STB',
            'operator_id' => 1,
            'uid' => $deviceUid,
            'subscriber_id' => null
        );
        $uniq = new UniqcastProvider;
        $queryParams = array('uid' => $deviceUid);
        return $uniq->connectDevice($uniqCastData, $queryParams);
    }

    public function deleteUser($subscriberUid)
    {
        $deleteData = array('uid' => $subscriberUid);
        $uniq = new UniqcastProvider;
        $uniq->deleteSubscriber($deleteData);
    }
    /**
     * Undocumented function
     *
     * @param [type] $subscriberId
     * @return void
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function getProfilesBySubscriberId($subscriberId)
    {
        $getSubscriberId = array('subscriber_id' => $subscriberId['data'][0]['id']);
        $uniq = new UniqcastProvider;
        return $uniq->getProfiles($getSubscriberId);
    }


    public function getProfilesByUid($profileUid)
    {

        $getUserProfile = array('uid' => $profileUid);
        $uniq = new UniqcastProvider;
        return $uniq->getProfiles($getUserProfile);
    }

    public function updateUserProfile($userProfileArray)
    {
        $uniqCastData = array(
            'uid' => $userProfileArray->profileUid,
            'operator_id' => 1,
            'language_id' => $userProfileArray->languageId,
            'name' => $userProfileArray->name,
            'refresh_token' => NULL,
            'main_pin' => $userProfileArray->authPin,
            'parental_pin' => $userProfileArray->parentalPin,
            'parental_rating' => $userProfileArray->parentalRating,
            'is_default' => $userProfileArray->isDefaultProfile,
            'audio_first_id' => NULL,
            'audio_second_id' => NULL,
            'subtitle_first_id' => NULL,
            'subtitle_second_id' => NULL,
        );

        $queryParams = array('uid' => $userProfileArray->profileUid);
        $uniq = new UniqcastProvider;
        return $uniq->updateProfiles($uniqCastData, $queryParams);
    }

    public function addPackageUser($packageId, $subscriberId)
    {

        $uniqCastData = array(
            'package_id' => $packageId,
            'subscriber_id' => $subscriberId,
            'operator_id' => 1
        );
        $uniq = new UniqcastProvider;
        return $uniq->purchasedPackage($uniqCastData);
    }
    public function removePackageUser($packageId, $subscriberId)
    {
        $deleteData = array(
            'subscriber_id' => $subscriberId,
            'package_id' => $packageId
        );
        $uniq = new UniqcastProvider;
        return $uniq->deleteSubscriberPackages($deleteData);
    }
}
