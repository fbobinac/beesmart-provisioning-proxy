<?php

namespace App\Providers\Beesmart;

use App\BeesmartUniqcast;
use Log;
use App\Models\Beesmart\User\CreateSubscriber;
use App\Models\Beesmart\User\ProfileUpdate;
use App\Models\Beesmart\User\GetProfile;
use App\Providers\UniqcastProvider;
use SoapFault;
use SoapClient;

use App\Soap\SoapUserModel;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\SoapController as NewSoap;

use App\Services\SoapService;


/**
 * Methods used by Demo service class.
 */
class BeesmartProvider
{

    /**
     * Return User Data
     *
     * @return \App\Models\Beesmart\User\CreateSubscriber
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function getUserData($statusCreatedUser)
    {


        if (isset($statusCreatedUser['status'])) {
            $beesmartUniqcast = new BeesmartUniqcast;
            $userDetails = $beesmartUniqcast->getUserById($statusCreatedUser);
            if (isset($userDetails['status'])) {
                $reponse = $beesmartUniqcast->getProfilesBySubscriberId($userDetails);
                $profileUid = $reponse['data'][0]['uid'];
            }
            $userData = $userDetails['data'][0];
            return new CreateSubscriber($userData['uid'], $userData['region_id'], $userData['first_name'], $userData['last_name'], $userData['status'], $userData['operator_id'], $profileUid, '', '', '', '', '', '', '', '', '');
        }
    }

    public function getUserPrfile($userProfile)
    {

        $beesmartUniqcast = new BeesmartUniqcast;
        $reponse = $beesmartUniqcast->getProfilesByUid($userProfile->profileUid);

        if (isset($reponse['data'][0])) {
            $profileParams = $reponse['data'][0];

            return new GetProfile($profileParams['subscriber_id'], $profileParams['uid'], $profileParams['name'], 'M', $profileParams['language_id'], '', '', $profileParams['main_pin'], $profileParams['parental_pin'], '', '', $profileParams['parental_rating'], $profileParams['parental_pin'], false, false, false, false, false, false, false, false, true);
        }
    }
    /**
     * check if user exist
     * checkc if device exist
     * TBD. response from beesmart when device is successfully added
     * @param string $deviceUid
     * @param string $subscriberUid
     * @return array
     */
    public function subscriberDevice($deviceUid, $subscriberUid)
    {
        // first check if user exist
        // getSubscriber
        $uniqcastProvider = new UniqcastProvider;
        $userDetails = $uniqcastProvider->getSubscriber(array('uid' => $subscriberUid));

        if (isset($userDetails['status'])) {
            if (isset($userDetails['data'][0]['id'])) {
                // then check if device exist
                $deviceDetails = $uniqcastProvider->getDevice(array('uid' => $deviceUid));

                if (isset($deviceDetails['status'])) {
                    // add device to user
                    $beesmartUniqcast = new BeesmartUniqcast;
                    $beesmartUniqcast->addUserDevice($deviceUid, $userDetails['data'][0]['id']);
                    return;
                }
                header("Status: 500");
                throw new SoapFault('SOAP-ENV:Client', 'Device dose not exist');
            }
        }
        header("Status: 500");
        throw new SoapFault('SOAP-ENV:Client', 'User dose not exist');
    }

    /**
     * Add specifict package to user
     * first check if user exist and
     * after check if package exist
     *
     * @param string $subscriberEntityUid
     * @param string $subscriptionUid
     * @return array
     */
    public function subscribeUserPackage($subscriberEntityUid, $subscriptionUid)
    {
        $uniqcastProvider = new UniqcastProvider;
        $userDetails = $uniqcastProvider->getSubscriber(array('uid' => $subscriptionUid));

        if (isset($userDetails['status'])) {
            if (isset($userDetails['data'][0]['id'])) {
                // then check if package exist
                $packageDetails = $uniqcastProvider->getPackages(array('uid' => $subscriberEntityUid));
                if (isset($packageDetails['status'])) {
                    // add device to user
                    $beesmartUniqcast = new BeesmartUniqcast;
                    $beesmartUniqcast->addPackageUser($packageDetails['data'][0]['id'], $userDetails['data'][0]['id']);
                    return;
                }
                header("Status: 401");
                throw new SoapFault('SOAP-ENV:Client', 'Package dose not exist');
            }
        }
        header("Status: 401");
        throw new SoapFault('SOAP-ENV:Client', 'User dose not exist');
    }

    /**
     * remove package from subscription
     *
     * @param string $subscriberEntityUid
     * @param string $subscriptionUid
     * @return void
     */
    public function removeSubscribeUserPackage($subscriberEntityUid, $subscriptionUid)
    {
        $uniqcastProvider = new UniqcastProvider;
        $userDetails = $uniqcastProvider->getSubscriber(array('uid' => $subscriptionUid));

        if (isset($userDetails['status'])) {
            if (isset($userDetails['data'][0]['id'])) {
                // then check if package exist
                $packageDetails = $uniqcastProvider->getPackages(array('uid' => $subscriberEntityUid));
                if (isset($packageDetails['status'])) {
                    // add device to user
                    $beesmartUniqcast = new BeesmartUniqcast;
                    $beesmartUniqcast->removePackageUser($packageDetails['data'][0]['id'], $userDetails['data'][0]['id']);
                    return;
                }
                header("Status: 401");
                throw new SoapFault('SOAP-ENV:Client', 'Package dose not exist');
            }
        }
        header("Status: 401");
        throw new SoapFault('SOAP-ENV:Client', 'User dose not exist');
    }


    public function getFilteredSubscribersBySoap($subscriberFilter)
    {

        $beesmartSoap = new SoapService('user');
        $params = $subscriberFilter;
        $soapRespo =  $beesmartSoap->soapCall('getFilteredSubscribers', $params);

        if (isset($soapRespo->subscriberResponseList)) {
            return  $soapRespo->subscriberResponseList;
        }

         return $soapRespo->faultcode;
    }

    public function getSubscriberBySaop($subscriberUid)
    {

        $beesmartSoap = new SoapService('user');
        $params = array('subscriberUid' => $subscriberUid);
        $soapRespo =  $beesmartSoap->soapCall('getSubscriber', $params);

        $soapRespo = $soapRespo->subscriberResponse;



        return array(
            'uid' => $soapRespo->subscriberUid,
            'region_id' => $soapRespo->regionUid,
            'first_name' => $soapRespo->name,
            'last_name' => isset($soapRespo->surname) ? $soapRespo->surname : '',
            'status' => $soapRespo->status,
            'operator_id' => 1,
            'profileUid' => $soapRespo->profileList->profileUid,
            'address' => isset($soapRespo->address) ? $soapRespo->address : '',
            'place' => isset($soapRespo->place) ? $soapRespo->place : null,
            'postalCode' => isset($soapRespo->postalCode) ? $soapRespo->postalCode : null,
            'post' => isset($soapRespo->post) ? $soapRespo->post : null,
            'country' => isset($soapRespo->country) ? $soapRespo->country : null
        );
    }

    public function createSubscriberBySaop($subscriberCreate)
    {
        $beesmartSoap = new SoapService('user');
        $params = new \stdClass();
        $params->subscriberCreate = $subscriberCreate;
        $soapRespo =  $beesmartSoap->soapCall('createSubscriber', $params);
        $soapRespo = $soapRespo->subscriberResponse;
        // Log::notice(print_r($soapRespo));
        // exit;
        return array(
            'uid' => $soapRespo->subscriberUid,
            'region_id' => $soapRespo->regionUid,
            'first_name' => $soapRespo->name,
            'last_name' => $soapRespo->surname,
            'status' => $soapRespo->status,
            'operator_id' => 1,
            'profileUid' => $soapRespo->profileList->profileUid,
            'address' => $soapRespo->address,
            'place' => isset($soapRespo->place) ? $soapRespo->place : null,
            'postalCode' => isset($soapRespo->postalCode) ? $soapRespo->postalCode : null,
            'post' => isset($soapRespo->post) ? $soapRespo->post : null,
            'country' => isset($soapRespo->country) ? $soapRespo->country : null
        );
    }

    public function deleteSubscriberBySaop($subscriberUid)
    {

        $beesmartSoap = new SoapService('user');
        $params = array('subscriberUid' => $subscriberUid);
        $soapRespo = $beesmartSoap->soapCall('deleteSubscriber', $params);
        if (isset($soapRespo->faultcode)) {
            header("Status: 500");
            throw new SoapFault($soapRespo->faultcode, $soapRespo->faultstring);
        }
        return;
    }

    public function getProfileSaop($profileUid)
    {

        $beesmartSoap = new SoapService('user');
        $params = array('profileUid' => $profileUid);
        $soapRespo = $beesmartSoap->soapCall('getProfile', $params);
        $soapRespo = $soapRespo->profileResponse;

        return new GetProfile(
            $soapRespo->subscriberUid,
            $soapRespo->profileUid,
            $soapRespo->name,
            $soapRespo->gender,
            $soapRespo->languageId,
            $soapRespo->skinId,
            $soapRespo->avatarId,
            $soapRespo->authPin,
            $soapRespo->userPin,
            $soapRespo->userPinValidTill,
            $soapRespo->birthDate,
            $soapRespo->parentalRating,
            $soapRespo->parentalPin,
            $soapRespo->autoLoginEnabled,
            $soapRespo->autoSubtitlesEnabled,
            $soapRespo->autoAudioEnabled,
            $soapRespo->tvRecommendationsEnabled,
            $soapRespo->sipPhoneNotificationsEnabled,
            $soapRespo->sipPhoneRedirect,
            $soapRespo->autoReminderTime,
            $soapRespo->isDefaultProfile
        );
    }


    public function updateSubscriberSaop($subscriberUpdate)
    {

        $beesmartSoap = new SoapService('user');
        $params = new \stdClass();
        $params->subscriberUpdate = $subscriberUpdate;
        $soapRespo = $beesmartSoap->soapCall('updateSubscriber', $params);
        $soapRespo = $soapRespo->subscriberResponse;

        return new CreateSubscriber(
            $soapRespo->subscriberUid,
            $soapRespo->regionUid,
            $soapRespo->name,
            $soapRespo->surname,
            $soapRespo->status,
            1,
            $soapRespo->profileList->profileUid,
            $soapRespo->address,
            $soapRespo->place,
            $soapRespo->postalCode,
            $soapRespo->post,
            $soapRespo->country,
            $soapRespo->fixedPhone,
            $soapRespo->mobilePhone,
            $soapRespo->workPhone,
            $soapRespo->fax,
            null
        );
    }

    public function updateProfileSaop($profileUpdate)
    {

        $beesmartSoap = new SoapService('user');
        $params = new \stdClass();
        $params->profileUpdate = $profileUpdate;
        $soapRespo = $beesmartSoap->soapCall('updateProfile', $params);
        $soapRespo = $soapRespo->profileResponse;

        return new GetProfile(
            $soapRespo->subscriberUid,
            $soapRespo->profileUid,
            $soapRespo->name,
            $soapRespo->gender,
            $soapRespo->languageId,
            $soapRespo->skinId,
            $soapRespo->avatarId,
            $soapRespo->authPin,
            $soapRespo->userPin,
            $soapRespo->userPinValidTill,
            $soapRespo->birthDate,
            $soapRespo->parentalRating,
            $soapRespo->parentalPin,
            $soapRespo->autoLoginEnabled,
            $soapRespo->autoSubtitlesEnabled,
            $soapRespo->autoAudioEnabled,
            $soapRespo->tvRecommendationsEnabled,
            $soapRespo->sipPhoneNotificationsEnabled,
            $soapRespo->sipPhoneRedirect,
            $soapRespo->autoReminderTime,
            $soapRespo->isDefaultProfile
        );
    }

    public function connectDeviceSaop($deviceUid, $subscriberUid)
    {

        $beesmartSoap = new SoapService('user');
        $params = array('deviceUid' => $deviceUid, 'subscriberUid' => $subscriberUid);
        $soapRespo = $beesmartSoap->soapCall('connectDevice', $params);
        if ($soapRespo->faultcode) {
            header("Status: 500");
            throw new SoapFault($soapRespo->faultcode, $soapRespo->faultstring);
        }
        return;
    }

    public function disconnectDeviceSaop($deviceUid)
    {

        $beesmartSoap = new SoapService('user');
        $params = array('deviceUid' => $deviceUid);
        $soapRespo = $beesmartSoap->soapCall('disconnectDevice', $params);
        if ($soapRespo->faultcode) {
            header("Status: 500");
            throw new SoapFault($soapRespo->faultcode, $soapRespo->faultstring);
        }
        return;
    }

    public function subscribeBySaop($subscriberEntityUid, $subscriptionUid)
    {
        $beesmartSoap = new SoapService('purchase');
        $params = array('subscriberEntityUid' => $subscriberEntityUid, 'subscriptionUid' => $subscriptionUid);
        $soapRespo = $beesmartSoap->soapCall('subscribe', $params);
        if ($soapRespo->faultcode) {
            header("Status: 500");
            throw new SoapFault($soapRespo->faultcode, $soapRespo->faultstring);
        }
        return;
    }

    public function unsubscribeBySaop($subscriberEntityUid, $subscriptionUid)
    {
        $beesmartSoap = new SoapService('purchase');
        $params = array('subscriberEntityUid' => $subscriberEntityUid, 'subscriptionUid' => $subscriptionUid);
        $soapRespo = $beesmartSoap->soapCall('unsubscribe', $params);
        if ($soapRespo->faultcode) {
            header("Status: 500");
            throw new SoapFault($soapRespo->faultcode, $soapRespo->faultstring);
        }
        return;
    }
}
