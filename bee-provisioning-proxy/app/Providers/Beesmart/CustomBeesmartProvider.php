<?php

namespace App\Providers\Beesmart;

use Log;
use Exception;


/**
 * Methods for custom action on beesmart iptv middleware
 */
class CustomBeesmartProvider
{


    public function customDeleteUsers()
    {

        $allUsers = $this->getAllUsers();
        $count = 0;
        $beesmartProvider = new BeesmartProvider;

        foreach ($allUsers->subscriberBasicResponse as $subscriber) {

            try {
                Log::notice($subscriber->subscriberUid . ' deleted'  );

                $beesmartProvider->deleteSubscriberBySaop($subscriber->subscriberUid);
                $count++;
                sleep(1);
                if ($count >= 750) {
                    break;
                }
            } catch (Exception $errorMeesages) {
                Log::notice('Error');
                Log::notice($errorMeesages);
                sleep(60);
            }
        }

    }

    public function getAllUsers()
    {
        $searchArray = array('subscriberFilter' => array('regionUid' => 'default'));

        $beesmartProvider = new BeesmartProvider;
        return $beesmartProvider->getFilteredSubscribersBySoap($searchArray);

        // Log::notice(json_encode($responseSearch));
        // foreach ($responseSearch->subscriberBasicResponse as $subscriber) {
        //     Log::notice(json_encode($subscriber));
        //     exit;
        // }
    }
}
