<?php

namespace App\Providers;


use App\Services\UniqcastService;
use function GuzzleHttp\json_encode;
use Illuminate\Support\Facades\Log;

/**
 * Methods used for Uniqcast REST API
 */
class UniqcastProvider
{
    /**
     * using for creating user
     * mandatory fields firstName, lastName,Uid, OperatorId, RegionId, Password
     *
     * @param array $userDataArray
     * @return void
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function createSubscriber($userDataArray)
    {
        Log::notice('Ulazak u UniqcastProvider::createSubscriber');

        if (
            isset($userDataArray['first_name']) && isset($userDataArray['last_name']) && isset($userDataArray['uid']) &&
            isset($userDataArray['password']) && isset($userDataArray['region_id']) && isset($userDataArray['operator_id'])
        ) {

            $uniqService = new UniqcastService;
            return $uniqService->subscribers($userDataArray, 'POST');
        }
    }
    /**
     * using for find user on platform
     * ['first_name' => 'Filip2']
     *
     * @param array $searchParams
     * @return void
     */
    public function getSubscriber($searchParams)
    {

        $uniqService = new UniqcastService;
        if (isset($searchParams)) {
            return $uniqService->subscribers($searchParams, 'GET');
        }
        // get all
        return $uniqService->subscribers(null, 'GET');
    }

    /**
     * using for deleting user
     * support only with ?uid=mtv
     * @param array $params
     * @return void
     */
    public function deleteSubscriber($params)
    {
        $uniqService = new UniqcastService;
        if (isset($params)) {
            $response = $uniqService->subscribers($params, 'DELETE');
        }
        return $response;
    }
    /**
     * update user
     *
     * @param array $params
     * @return void
     */
    public function updateSubscriber($params, $queryParams = null)
    {
        $uniqService = new UniqcastService;
        if (isset($params)) {
            $response = $uniqService->subscribers($params, 'PUT', $queryParams);
        }
        return $response;
    }

    /**
     * get devices
     * search by parametar or get all device
     *
     * example. ['uid' => '7b05e9460c6a0cda']
     * @param array $searchParams
     * @return void
     */
    public function getDevice($searchParams = null)
    {

        $uniqService = new UniqcastService;
        if (isset($searchParams)) {
            return $uniqService->devices($searchParams, 'GET');
        }
        return $uniqService->devices(null, 'GET');
    }

    public function connectDevice($params, $queryParams = null)
    {

        $uniqService = new UniqcastService;
        if (isset($params)) {
            // $upadteArray = array('uid' => $uid, 'subscriber_id' => $subscriberId );
            // return $uniqService->devices($upadteArray, 'PUT', $queryParams);
            return $uniqService->devices($params, 'PUT', $queryParams);
        }
    }

    public function disconnectDevice($deviceId, $uid)
    {

        $uniqService = new UniqcastService;
        if (isset($uid) && isset($deviceId)) {
            $upadteArray = array('id' => $deviceId, 'uid' => $uid, 'subscriber_id' => null);
            return $uniqService->devices($upadteArray, 'PUT');
        }
    }

    public function createProfiles($uid, $operatorId, $subscriberId, $languageId, $name, $mainPin, $parentalPin, $parentalRating, $isDefault, $audioFirstId, $audioSecondId, $subtitleFirstId, $subtitleSecondId)
    {

        $createArray = array(
            'uid' => $uid,
            'operator_id' => $operatorId,
            'subscriber_id' => $subscriberId,
            'language_id' => $languageId,
            'name' => $name,
            'main_pin' => $mainPin,
            'parental_pin' => isset($parentalPin) ? $parentalPin : null,
            'parental_rating' => isset($parentalRating) ? $parentalRating : null,
            'is_default' => isset($isDefault) ? $isDefault : null,
            'audio_first_id' => isset($audioFirstId) ? $audioFirstId : null,
            'audio_second_id' => isset($audioSecondId) ? $audioSecondId : null,
            'subtitle_first_id' => isset($subtitleFirstId) ? $subtitleFirstId : null,
            'subtitle_second_id' => isset($subtitleSecondId) ? $subtitleSecondId : null,
        );
        $uniqService = new UniqcastService;
        $uniqService->profiles($createArray, 'POST');
    }

    public function getProfiles($searchParams)
    {
        $uniqService = new UniqcastService;
        if (isset($searchParams)) {
            return $uniqService->profiles($searchParams, 'GET');
        }
        // get all
        return $uniqService->profiles(null, 'GET');
    }


    /**
     * using for Profiles user
     * support only with ?uid=mtv
     * @param array $params
     * @return void
     */
    public function deleteProfiles($params)
    {
        $uniqService = new UniqcastService;
        if (isset($params)) {
            $response = $uniqService->profiles($params, 'DELETE');
        }
        return $response;
    }
    /**
     * update Profiles
     *
     * @param array $params
     * @return void
     */
    public function updateProfiles($params, $queryParams = null)
    {
        $uniqService = new UniqcastService;
        if (isset($params)) {
            $response = $uniqService->profiles($params, 'PUT', $queryParams);
        }
        return $response;
    }

    /**
     * get purchase packages
     *
     * @param array $searchParams
     * @return array
     */
    public function getPurchasedPackages($searchParams)
    {
        $uniqService = new UniqcastService;
        if (isset($searchParams)) {
            return $uniqService->purchasedPackages($searchParams, 'GET');
        }
        // get all
        return $uniqService->purchasedPackages(null, 'GET');
    }
    /**
     * add package to subscriber
     *
     * @param array $params
     * @return array
     */
    public function purchasedPackage($params)
    {
        $uniqService = new UniqcastService;
        if (isset($params)) {
            $response = $uniqService->subscriberPackages($params, 'POST');
        }
        return $response;
    }

     /**
     * remove package from subscriber
     * support only with ?uid=mtv
     * @param array $params
     * @return array
     */
    public function deleteSubscriberPackages($params)
    {
        $uniqService = new UniqcastService;
        if (isset($params)) {
            $response = $uniqService->subscriberPackages($params, 'DELETE');
        }
        return $response;
    }


    /**
     * Get all Packages or wanted package
     *
     * @param array $params
     * @return array
     */
    public function getPackages($params)
    {
        $uniqService = new UniqcastService;
        if (isset($params)) {
            $response = $uniqService->packages($params, 'GET');
        }
        return $response;
    }
}
