<?php

namespace App\Services;


use SoapFault;
use Log;
use App\Providers\Beesmart\BeesmartProvider;
use Config;

/**
 * An example of a class that is used as a SOAP gateway to application functions.
 */
class PurchaseBeesmartService
{

    // /**
    //  * Undocumented function
    //  *
    //  * @param string $billingSubscriberUid
    //  * @param string $consumingSubscriberUid
    //  * @param string $purchaseGroupUid
    //  * @param string $priceListItemUid
    //  * @param int $offerVersion
    //  * @return string
    //  * @throws SoapFault
    //  */
    // public function purchaseProduct($billingSubscriberUid,$consumingSubscriberUid,$purchaseGroupUid,$priceListItemUid,$offerVersion){

    // }

    // /**
    //  * Undocumented function
    //  *
    //  * @param string $regionName
    //  * @return string
    //  * @throws SoapFault
    //  */
    // public function listPurchaseGroups($regionName){

    // }

    // /**
    //  * Undocumented function
    //  *
    //  * @param string $subscriptionUid
    //  * @param string $purchaseGroupUid
    //  * @return string
    //  * @throws SoapFault
    //  */
    // public function getProduct($subscriptionUid,$purchaseGroupUid){

    // }

    // /**
    //  * Undocumented function
    //  *
    //  * @param string $priceListUid
    //  * @return string
    //  * @throws SoapFault
    //  */
    // public function getPriceList($priceListUid){

    // }

    // /**
    //  * Undocumented function
    //  *
    //  * @param \App\Models\Beesmart\Purchase\GetFilteredProductPurchases $priceProductPurchasesFilter
    //  * @return string
    //  * @throws SoapFault
    //  */
    // public function getFilteredProductPurchases($priceProductPurchasesFilter){

    //     // Log::info(json_encode($priceProductPurchasesFilter));
    //     // Log::info($this->arrayOfKeyValueToArray($priceProductPurchasesFilter));

    //     Log::info(($priceProductPurchasesFilter));
    // }

    // /**
    //  * Undocumented function
    //  *
    //  * @param string $subscriberUid
    //  * @param string $purchaseGroupUid
    //  * @param string $priceListItemUid
    //  * @return string
    //  * @throws SoapFault
    //  */
    // public function revokeProductPurchase($subscriberUid, $purchaseGroupUid,$priceListItemUid){


    // }

    /**
     * Add Package to Subscriber
     *
     * @param string $subscriberEntityUid
     * @param string $subscriptionUid
     * @return string
     * @throws SoapFault
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function subscribe($subscriberEntityUid, $subscriptionUid)
    {

        if (isset($subscriberEntityUid) && isset($subscriptionUid)) {

            if (Config::get('uniqcast.beesmart.is_enabled')) {
                $beesmartProvider = new BeesmartProvider;
                return $beesmartProvider->subscribeBySaop($subscriberEntityUid, $subscriptionUid);
            }

            $subscribe = new BeesmartProvider;
            return $subscribe->subscribeUserPackage($subscriberEntityUid, $subscriptionUid);

        }

        header("Status: 401");
        throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
    }

     /**
     * Remove Package from Subscriber
     *
     * @param string $subscriberEntityUid
     * @param string $subscriptionUid
     * @return string
     * @throws SoapFault
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function unsubscribe($subscriberEntityUid, $subscriptionUid)
    {

        if (isset($subscriberEntityUid) && isset($subscriptionUid)) {

            if (Config::get('uniqcast.beesmart.is_enabled')) {
                $beesmartProvider = new BeesmartProvider;
                return $beesmartProvider->unsubscribeBySaop($subscriberEntityUid, $subscriptionUid);
            }

            $subscribe = new BeesmartProvider;
            return $subscribe->removeSubscribeUserPackage($subscriberEntityUid, $subscriptionUid);

        }

        header("Status: 401");
        throw new SoapFault('SOAP-ENV:Client', 'Incorrect credentials.');
    }

}
