<?php

namespace App\Services;

use GuzzleHttp\Client;

use Config;
use Log;

/**
 * An example of a class that is used as a SOAP gateway to application functions.
 */
class UniqcastService
{

    /**
     * add/edit/remove users
     *
     * @param array $inputArray
     * @param string $requestType
     * @param array $queryParams
     * @return json
     */
    public function subscribers($inputArray = null, $requestType = 'GET', $queryParams = null)
    {
        // Create a client with a base URI
        $client = new Client(['base_uri' => config('uniqcast.url')]);

        if (!is_null($inputArray) && $requestType === 'GET') {
            $response = $client->request(
                $requestType,
                '/api/v1/subscribers',
                [
                    'auth'  => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $inputArray
                ]
            );
        } elseif (!is_null($inputArray) && $requestType === 'POST') {
            $response = $client->request(
                $requestType,
                '/api/v1/subscribers',
                [
                    'auth' => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'json' => $inputArray
                ]
            );
        } else if (!is_null($inputArray) && $requestType === 'DELETE') {
            $response = $client->request(
                $requestType,
                '/api/v1/subscribers',
                [
                    'auth' => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $inputArray
                ]
            );
        } else if (!is_null($inputArray) && $requestType === 'PUT') {
            $response = $client->request(
                $requestType,
                '/api/v1/subscribers',
                [
                    'auth' => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $queryParams,
                    'json' => $inputArray
                ]
            );
        }

        $responseJson = json_decode($response->getBody()->getContents(), true);
        return $responseJson;
    }

    /**
     * get/add device
     *
     * @param array $inputArray
     * @param string $requestType
     * @param array $queryParams
     * @return json
     */
    public function devices($inputArray = null, $requestType = 'GET', $queryParams = null)
    {

        $client = new Client(['base_uri' => config('uniqcast.url')]);

        if (!is_null($inputArray) && $requestType === 'GET') {

            $response =  $client->request(
                $requestType,
                '/api/v1/devices',
                [
                    'auth'  => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $inputArray
                ]
            );
        } elseif (!is_null($inputArray) && $requestType === 'POST') {
            $response = $client->request(
                $requestType,
                '/api/v1/devices',
                [
                    'auth' => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'json' => $inputArray
                ]
            );
        } else if (!is_null($inputArray) && $requestType === 'DELETE') {
            $response = $client->request(
                $requestType,
                '/api/v1/devices',
                [
                    'auth' => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $inputArray
                ]
            );
        } else if (!is_null($inputArray) && $requestType === 'PUT') {
            $response = $client->request(
                $requestType,
                '/api/v1/devices',
                [
                    'auth' => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $queryParams,
                    'json' => $inputArray
                ]
            );
        }

        $responseJson = json_decode($response->getBody()->getContents(), true);
        return $responseJson;
    }

    /**
     * get users profile details
     *
     * @param array $inputArray
     * @param string $requestType
     * @param array $queryParams
     * @return json
     */
    public function profiles($inputArray = null, $requestType = 'GET', $queryParams = null)
    {

        $client = new Client(['base_uri' => config('uniqcast.url')]);

        if (!is_null($inputArray) && $requestType === 'GET') {

            $response = $client->request(
                $requestType,
                '/api/v1/profiles',
                [
                    'auth'  => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $inputArray
                ]
            );
        } elseif (!is_null($inputArray) && $requestType === 'POST') {
            $response = $client->request(
                $requestType,
                '/api/v1/profiles',
                [
                    'auth' => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'json' => $inputArray
                ]
            );
        } else if (!is_null($inputArray) && $requestType === 'DELETE') {
            $response = $client->request(
                $requestType,
                '/api/v1/profiles',
                [
                    'auth' => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $inputArray
                ]
            );
        } else if (!is_null($inputArray) && $requestType === 'PUT') {
            $response = $client->request(
                $requestType,
                '/api/v1/profiles',
                [
                    'auth' => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $queryParams,
                    'json' => $inputArray
                ]
            );
        }

        $responseJson = json_decode($response->getBody()->getContents(), true);
        return $responseJson;
    }

    /**
     * using for buying tv package for users
     *
     * @param array $inputArray
     * @param string $requestType
     * @return void
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function purchasedPackages($inputArray = null, $requestType = 'GET')
    {

        $client = new Client(['base_uri' => config('uniqcast.url')]);

        if (!is_null($inputArray) && $requestType === 'GET') {

            $response = $client->request(
                $requestType,
                '/api/v1/purchased_packages',
                [
                    'auth'  => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $inputArray
                ]
            );
        } else if (!is_null($inputArray) && $requestType === 'PUT') {
            echo 'entered in update';
            $response = $client->request(
                $requestType,
                '/api/v1/profiles',
                [
                    'auth' => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $inputArray
                ]
            );
        }

        echo ($response->getBody()->getContents());
        $responseJson = json_decode($response->getBody()->getContents(), true);

        return $responseJson;
    }

    /**
     * get users packages, add new packages or edit existing packages
     *
     * @param array $inputArray
     * @param string $requestType
     * @param array $queryParams
     * @return json
     */
    public function packages($inputArray = null, $requestType = 'GET', $queryParams = null)
    {

        $client = new Client(['base_uri' => config('uniqcast.url')]);

        if (!is_null($inputArray) && $requestType === 'GET') {
            $response = $client->request(
                $requestType,
                '/api/v1/packages',
                [
                    'auth'  => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $inputArray
                ]
            );
        } elseif (!is_null($inputArray) && $requestType === 'POST') {
            $response = $client->request(
                $requestType,
                '/api/v1/packages',
                [
                    'auth' => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'json' => $inputArray
                ]
            );
        } else if (!is_null($inputArray) && $requestType === 'DELETE') {
            $response = $client->request(
                $requestType,
                '/api/v1/packages',
                [
                    'auth' => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $inputArray
                ]
            );
        } else if (!is_null($inputArray) && $requestType === 'PUT') {
            $response = $client->request(
                $requestType,
                '/api/v1/packages',
                [
                    'auth' => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $queryParams,
                    'json' => $inputArray
                ]
            );
        }

        $responseJson = json_decode($response->getBody()->getContents(), true);
        return $responseJson;
    }

    /**
     * Mapping package to user
     *
     * @param array $inputArray
     * @param string $requestType
     * @param array $queryParams
     * @return json
     */
    public function subscriberPackages($inputArray = null, $requestType = 'GET', $queryParams = null)
    {

        $client = new Client(['base_uri' => config('uniqcast.url')]);

        if (!is_null($inputArray) && $requestType === 'GET') {
            $response = $client->request(
                $requestType,
                '/api/v1/subscriber_packages',
                [
                    'auth'  => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $inputArray
                ]
            );
        } elseif (!is_null($inputArray) && $requestType === 'POST') {
            $response = $client->request(
                $requestType,
                '/api/v1/subscriber_packages',
                [
                    'auth' => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'json' => $inputArray
                ]
            );
        } else if (!is_null($inputArray) && $requestType === 'DELETE') {
            $response = $client->request(
                $requestType,
                '/api/v1/subscriber_packages',
                [
                    'auth' => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $inputArray
                ]
            );
        } else if (!is_null($inputArray) && $requestType === 'PUT') {
            $response = $client->request(
                $requestType,
                '/api/v1/subscriber_packages',
                [
                    'auth' => [config('uniqcast.auth.username'), config('uniqcast.auth.password')],
                    'query' => $queryParams,
                    'json' => $inputArray
                ]
            );
        }

        $responseJson = json_decode($response->getBody()->getContents(), true);
        return $responseJson;
    }
}
