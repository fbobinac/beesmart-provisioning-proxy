<?php

namespace App\Services;


use Log;
use SoapFault;
use SoapClient;
use Config;


/**
 * Methods using for Beesmart SOAP WSDL
 */
class SoapService
{

    /**
     * @var string
     */
    protected $wsdl;

    /**
     * @var string
     */
    protected $location;

    /**
     * @var string
     */
    protected $serviceName;

    /**
     * @var array
     */
    protected $streamContext;

    /**
     * @var boolean
     */
    protected $trace = false;

    public function __construct($serviceName)
    {
        $this->serviceNam = $serviceName;
        // Log::notice(Config::get('uniqcast.beesmart.' . $serviceName));
        if (Config::get('uniqcast.beesmart.' . $serviceName)) {
            $this->wsdl = Config::get('uniqcast.beesmart.url') . Config::get('uniqcast.beesmart.' . $serviceName) . '?WSDL';
            if (Config::get('uniqcast.beesmart.target.url') !== null) {
                $this->location = Config::get('uniqcast.beesmart.target.url') . Config::get('uniqcast.beesmart.' . $serviceName);
            }

            if (Config::get('uniqcast.beesmart.ssl.is_enabled')) {
                $this->streamContext =  Config::get('uniqcast.beesmart.ssl.stream_context');
            }
            $this->trace = true;
        } else {

            throw new SoapFault('SOAP-ENV:Client', 'Module Dose not exist');
        }
    }


    public function soapCall($nameOfFunction, $data)
    {

        $serviceClient = new SoapClient($this->wsdl, array(
            "trace" => $this->trace,
            "exceptions" => 0,
            "connection_timeout" => 2000,
            "location" => $this->location,
            'stream_context' => $this->streamContext,
        ));


        return $serviceClient->__soapCall($nameOfFunction, [$data]);
    }
}
