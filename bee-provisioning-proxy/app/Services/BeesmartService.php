<?php

namespace App\Services;


use SoapFault;
use App\Models\Beesmart\User\CreateSubscriber;
use App\Providers\Beesmart\BeesmartProvider;
use App\Models\Beesmart\User\GetProfile;
use App\BeesmartUniqcast;
use Log;
use Config;

/**
 * An example of a class that is used as a SOAP gateway to application functions.
 */
class BeesmartService
{

    /**
     * Create new user
     *
     * @param \App\Models\Beesmart\User\CreateSubscriber[[]] $subscriberCreate
     * @return \App\Models\Beesmart\User\CreateSubscriber
     * @throws SoapFault
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function createSubscriber($subscriberCreate)
    {

        if (isset($subscriberCreate)) {

            if (Config::get('uniqcast.beesmart.is_enabled')) {
                Log::notice('Beesmart');
                Log::notice(print_r($subscriberCreate));
                $beesmartProvider = new BeesmartProvider;
                $userData = $beesmartProvider->createSubscriberBySaop($subscriberCreate);
                return new CreateSubscriber($userData['uid'], $userData['region_id'], $userData['first_name'], $userData['last_name'], $userData['status'], $userData['operator_id'], $userData['profileUid'], $userData['address'], 0, 0, 0, $userData['country']);
            }

            $beesmartUniqcast = new BeesmartUniqcast;
            $statusCreatedUser = $beesmartUniqcast->createUser($subscriberCreate);
            $profileUid = '';
            // get subscrber detalis after we created user
            // and profile uid
            if (isset($statusCreatedUser['status'])) {
                $userDetails = $beesmartUniqcast->getUserById($statusCreatedUser);
                if (isset($userDetails['status'])) {
                    $reponse = $beesmartUniqcast->getProfilesBySubscriberId($userDetails);
                    $profileUid = $reponse['data'][0]['uid'];
                }

                $userData = $userDetails['data'][0];
                return new CreateSubscriber($userData['uid'], $userData['region_id'], $userData['first_name'], $userData['last_name'], $userData['status'], $userData['operator_id'], $profileUid, '', '', '', '', '', '', '', '', '');
            }
            header("Status: 400");
            throw new SoapFault('SOAP-ENV:Client', 'Missing Parametars');
        }
        header("Status: 400");
        throw new SoapFault('SOAP-ENV:Client', 'Missing input parametars');
    }


    /**
     * Deleting subscriber from IPTV Platform
     *
     * @param string $subscriberUid
     * @return string
     * @throws SoapFault
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function deleteSubscriber($subscriberUid)
    {

        if (isset($subscriberUid)) {

            if (Config::get('uniqcast.beesmart.is_enabled')) {
                $beesmartProvider = new BeesmartProvider;
                return $beesmartProvider->deleteSubscriberBySaop($subscriberUid);
            }
            $beesmartUniqcast = new BeesmartUniqcast;
            $beesmartUniqcast->deleteUser($subscriberUid);
            return;
        }

        header("Status: 401");
        throw new SoapFault('SOAP-ENV:Server', 'Subscriber not found');
    }
    /**
     * Get subbscriber profile parametars
     *
     * @param string $profileUid
     * @return \App\Models\Beesmart\User\GetProfile
     * @throws SoapFault
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function getProfile($profileUid)
    {

        if (isset($profileUid)) {

            if (Config::get('uniqcast.beesmart.is_enabled')) {
                $beesmartProvider = new BeesmartProvider;
                return $beesmartProvider->getProfileSaop($profileUid);
            }

            $beesmartUniqcast = new BeesmartUniqcast;
            $reponse = $beesmartUniqcast->getProfilesByUid($profileUid);
            if (isset($reponse['data'][0])) {
                $profileParams = $reponse['data'][0];
                return new GetProfile($profileParams['subscriber_id'], $profileParams['uid'], $profileParams['name'], 'M', $profileParams['language_id'], '', '', $profileParams['main_pin'], $profileParams['main_pin'], '', '', $profileParams['parental_rating'], $profileParams['parental_pin'], false, false, false, false, false, false, false, false, true);
            }
            header("Status: 400");
            throw new SoapFault('SOAP-ENV:Client', 'User dose not exist');
        }
        header("Status: 400");
        throw new SoapFault('SOAP-ENV:Client', 'Please specify profileUid.');
    }

    /**
     * Update subscruber parametars
     *
     * @param \App\Models\Beesmart\User\CreateSubscriber $subscriberUpdate
     * @return \App\Models\Beesmart\User\CreateSubscriber
     * @throws SoapFault
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function updateSubscriber($subscriberUpdate)
    {
        if (isset($subscriberUpdate)) {

            if (Config::get('uniqcast.beesmart.is_enabled')) {
                $beesmartProvider = new BeesmartProvider;
                return $beesmartProvider->updateSubscriberSaop($subscriberUpdate);
            }

            $beesmartUniqcast = new BeesmartUniqcast;
            $statusUpdatedUser = $beesmartUniqcast->updateUser($subscriberUpdate);
            if (isset($statusUpdatedUser['status'])) {
                $getUser = new BeesmartProvider;
                return $getUser->getUserData($statusUpdatedUser);
            }
            header("Status: 400");
            throw new SoapFault('SOAP-ENV:Client', 'Problem while update User Data');
        }

        header("Status: 400");
        throw new SoapFault('SOAP-ENV:Client', 'Please specify Subscriber.');
    }

    /**
     * Update subscriber profile parametars
     *
     * @param \App\Models\Beesmart\User\ProfileUpdate[[]] $profileUpdate
     * @return \App\Models\Beesmart\User\GetProfile
     * @throws SoapFault
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function updateProfile($profileUpdate)
    {
        if (isset($profileUpdate)) {

            if (Config::get('uniqcast.beesmart.is_enabled')) {
                $beesmartProvider = new BeesmartProvider;
                return $beesmartProvider->updateProfileSaop($profileUpdate);
            }

            $beesmartUniqcast = new BeesmartUniqcast;
            $statusUpdateProfile = $beesmartUniqcast->updateUserProfile($profileUpdate);

            if (isset($statusUpdateProfile['status'])) {
                $getUser = new BeesmartProvider;
                return $getUser->getUserPrfile($profileUpdate);
            }
        }
        header("Status: 400");
        throw new SoapFault('SOAP-ENV:Client', 'Please specify profileUpdate.');
    }

    /**
     * Get subscriber details
     *
     * @param string $subscriberUid
     * @return \App\Models\Beesmart\User\CreateSubscriber
     * @throws SoapFault
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function getSubscriber($subscriberUid)
    {

        if (isset($subscriberUid)) {
            // when soap call are going to beesmart
            if (Config::get('uniqcast.beesmart.is_enabled')) {
                $beesmartProvider = new BeesmartProvider;
                $userData = $beesmartProvider->getSubscriberBySaop($subscriberUid);
                return new CreateSubscriber($userData['uid'], $userData['region_id'], $userData['first_name'], $userData['last_name'], $userData['status'], $userData['operator_id'], $userData['profileUid'], $userData['address'], 0, 0, 0, $userData['country']);
            }
            $beesmartUniqcast = new BeesmartUniqcast;
            $userDetails = $beesmartUniqcast->getSubscriberByUid($subscriberUid);
            if (isset($userDetails['status'])) {
                $reponse = $beesmartUniqcast->getProfilesBySubscriberId($userDetails);
                $profileUid = $reponse['data'][0]['uid'];
            }
            $userData = $userDetails['data'][0];
            return new CreateSubscriber($userData['uid'], $userData['region_id'], $userData['first_name'], $userData['last_name'], $userData['status'], $userData['operator_id'], $profileUid, '', '', '', '', '', '', '', '', '');
        }
        header("Status: 400");
        throw new SoapFault('SOAP-ENV:Client', 'Missing Parametars');
    }


    /**
     * Connect devoice to subscriber
     *
     * @param string $deviceUid
     * @param string $subscriberUid
     * @return string
     * @throws SoapFault
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function connectDevice($deviceUid, $subscriberUid)
    {

        if (isset($deviceUid) && isset($subscriberUid)) {

            if (Config::get('uniqcast.beesmart.is_enabled')) {
                $beesmartProvider = new BeesmartProvider;
                return $beesmartProvider->connectDeviceSaop($deviceUid, $subscriberUid);
            }

            $beesmartProvider = new BeesmartProvider;
            return $beesmartProvider->subscriberDevice($deviceUid, $subscriberUid);
        }

        header("Status: 500");
        throw new SoapFault('SOAP-ENV:Client', 'Missing Parametars');
    }

    /**
     * Disconnect device from subscriber
     *
     * @param string $deviceUid
     * @return string
     * @throws SoapFault
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function disconnectDevice($deviceUid)
    {
        if (isset($deviceUid)) {

            if (Config::get('uniqcast.beesmart.is_enabled')) {
                $beesmartProvider = new BeesmartProvider;
                return $beesmartProvider->disconnectDeviceSaop($deviceUid);
            }

            $beesmartUniqcast = new BeesmartUniqcast;
            $beesmartUniqcast->removeUserDevice($deviceUid);
            return;
        }

        header("Status: 401");
        throw new SoapFault('SOAP-ENV:Client', 'Missing Device');
    }


    /*
    |--------------------------------------------------------------------------
    | Utility
    |--------------------------------------------------------------------------
    */

    /**
     * Convert array of KeyValue objects to associative array, non-recursively.
     *
     * @param \Viewflex\Zoap\Demo\Types\KeyValue[] $objects
     * @return array
     */
    protected static function arrayOfKeyValueToArray($objects)
    {
        $return = array();
        foreach ($objects as $object) {
            $return[$object->key] = $object->value;
        }

        return $return;
    }
}
