<?php

namespace App\Models\Beesmart\Purchase;


class GetFilteredProductPurchases
{
    /**
     * @var string
     */
    public $billingSubscriberUid;

    /**
     * @var string
     */
    public $consumingSubscriberUid;

    /**
    * @var \App\Models\Beesmart\Purchase\TimeOfPurchaseRange
    */
    public $timeOfPurchaseRange;


    /**
    * @var \App\Models\Beesmart\Purchase\ValidityPeriod
    */
    public $validityPeriod;

    /**
     * @var string
     */
    public $paymentType;

    /**
     * @var string
     */
    public $purchaseGroupUid;

    /**
     * @var string
     */
    public $priceListItemUid;

    /**
     * @var string
     */
    public $subscriptionUid;

    /**
     * @var string
     */
    public $price;

    /**
     * @var int
     */
    public $priceType;

    /**
     * @var string (optional)
     */
    public $billingType;


}

class TimeOfPurchaseRange {

    /**
     * @var string
     */
    public $start;

    /**
     * @var string
     */
    public $end;

}

class ValidityPeriod {

    /**
     * @var string
     */
    public $start;

    /**
     * @var string
     */
    public $end;

}

