<?php

namespace App\Models\Beesmart\User;


class GetProfile
{


    /**
     * @var string
     */
    public $subscriberUid;
    /**
     * @var string
     */
    public $profileUid;
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $gender;
    /**
     * @var int
     */
    public $languageId;
    /**
     * @var int
     */
    public $skinId;
    /**
     * @var int
     */
    public $avatarId;

    /**
     * @var string
     */
    public $authPin;

    /**
     * @var string
     */
    public $userPin;

    /**
     * @var string
     */
    public $userPinValidTill;

    /**
     * @var string
     */
    public $birthDate;
    /**
     * @var string
     */
    public $parentalRating;
    /**
     * @var string
     */
    public $parentalPin;
    /**
     * @var bool
     */
    public $autoLoginEnabled;
    /**
     * @var bool
     */
    public $autoSubtitlesEnabled;
    /**
     * @var bool
     */
    public $autoAudioEnabled;
    /**
     * @var bool
     */
    public $tvRecommendationsEnabled;
    /**
     * @var bool
     */
    public $vodRecommendationsEnabled;
    /**
     * @var bool
     */
    public $sipPhoneNotificationsEnabled;

    /**
     * @var string
     */
    public $sipPhoneRedirect;
    /**
     * @var string
     */
    public $autoReminderTime;
    /**
     * @var bool
     */
    public $isDefaultProfile;

    /**
     * @var bool
     */
    public $hasUserLoggedIn;

    public function __construct($subscriberUid,$profileUid, $name = '', $gender = '', $languageId = '', $skinId = '', $avatarId = '', $authPin = '', $userPin = '', $userPinValidTill = '', $birthDate = '', $parentalRating = '', $parentalPin = '', $autoLoginEnabled = '', $autoSubtitlesEnabled = '', $autoAudioEnabled = '', $tvRecommendationsEnabled = '', $vodRecommendationsEnabled = '', $sipPhoneNotificationsEnabled = '', $sipPhoneRedirect = '', $autoReminderTime = '', $isDefaultProfile = '')
    {
        $this->subscriberUid = $subscriberUid;
        $this->profileUid = $profileUid;
        $this->name = $name;
        $this->gender = $gender;
        $this->languageId = $languageId;

        $this->skinId = $skinId;
        $this->avatarId = $avatarId;
        $this->authPin = $authPin;

        $this->userPin = $userPin;
        $this->userPinValidTill = $userPinValidTill;
        $this->birthDate = $birthDate;
        $this->parentalRating = $parentalRating;
        $this->parentalPin = $parentalPin;
        $this->autoLoginEnabled = $autoLoginEnabled;

        $this->autoSubtitlesEnabled = $autoSubtitlesEnabled;
        $this->autoAudioEnabled = $autoAudioEnabled;
        $this->tvRecommendationsEnabled = $tvRecommendationsEnabled;
        $this->vodRecommendationsEnabled = $vodRecommendationsEnabled;
        $this->sipPhoneNotificationsEnabled = $sipPhoneNotificationsEnabled;

        $this->sipPhoneRedirect = $sipPhoneRedirect;
        $this->autoReminderTime = $autoReminderTime;
        $this->isDefaultProfile = $isDefaultProfile;
        $this->hasUserLoggedIn = true;
    }
}
