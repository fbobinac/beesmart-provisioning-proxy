<?php

namespace App\Models\Beesmart\User;


class CreateSubscriber
{
    /**
     * @var string
     */
    public $subscriberUid;

    /**
     * @var string
     */
    public $regionUid;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $surname;

    /**
     * @var string
     */
    public $status;

    /**
     * @var int
     */
    public $operatorId;

    /**
    * @var \App\Models\Beesmart\User\ProfileList
    */
    public $profileList;

    /**
     * @var string
     */
    public $address;

    /**
     * @var string
     */
    public $place;

    /**
     * @var int
     */
    public $postalCode;

    /**
     * @var string (optional)
     */
    public $post;

    /**
     * @var string (optional)
     */
    public $country;

    /**
     * @var int
     */
    public $fixedPhone;

    /**
     * @var int
     */
    public $mobilePhone;

    /**
     * @var int
     */
    public $workPhone;

    /**
     * @var int
     */
    public $fax;

    /**
    * @var \App\Models\Beesmart\User\IpAddressList
    */
    public $ipAddressList;

    public function __construct($subscriberUid,$regionUid,$name='', $surname='',$status,$operatorId='',$profileList='',$address='',$place,$postalCode='',$post='',$country='',$fixedPhone='',$mobilePhone='',$workPhone='',$fax='', $ipAddressList='' ){
        $this->subscriberUid = $subscriberUid;
        $this->regionUid = $regionUid;
        $this->name = $name;
        $this->surname = $surname;
        $this->status = $status;
        $this->operatorId = $operatorId;
        $this->address=$address;
        $this->place =$place;
        $this->postalCode = $postalCode;
        $this->post = $post;
        $this->country = $country;
        $this->fixedPhone = $fixedPhone;
        $this->mobilePhone = $mobilePhone;
        $this->workPhone = $workPhone;
        $this->fax = $fax;
        $this->ipAddressList = $ipAddressList;

        $this->profileList = new ProfileList($profileList);

    }

}

class IpAddressList {

    /**
     * @var string
     */
    public $ipAddress;

    public function __construct($ipAddress=''){
        $this->ipAddress = $ipAddress;

    }


}

class ProfileList{
    /**
     * @var string
     */
    public $profileUid;

    public function __construct($profileUid=''){
        $this->profileUid = $profileUid;

    }

}
