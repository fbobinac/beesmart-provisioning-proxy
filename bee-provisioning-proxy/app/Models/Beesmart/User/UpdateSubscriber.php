<?php

namespace App\Models\Beesmart\User;


class UpdateSubscriber
{

    /**
     * @var string
     */
    public $subscriberUid;

    /**
     * @var string
     */
    public $regionUid;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $surname;

    /**
     * @var string
     */
    public $status;

    /**
     * @var string
     */
    public $address;

    /**
     * @var string
     */
    public $place;

    /**
     * @var int
     */
    public $postalCode;

    /**
     * @var string
     */
    public $post;

    /**
     * @var string
     */
    public $country;

    /**
     * @var int
     */
    public $fixedPhone;

    /**
     * @var int
     */
    public $mobilePhone;

    /**
     * @var int
     */
    public $workPhone;

    /**
     * @var int
     */
    public $fax;

    /**
    * @var \App\Models\Beesmart\User\IpAddressList
    */
    public $ipAddressList;

}
