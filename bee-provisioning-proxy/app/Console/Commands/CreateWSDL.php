<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Log;
use Storage;
use Config;

class CreateWSDL extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wsdl:save';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create WSDL file on disk for problem with docker';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if (strlen(config('uniqcast.serverName')) !== 0) {

            $wsdl = file_get_contents('http://' . config('uniqcast.serverName') . '/api/cwsNew/userBeesmart/server?wsdl');
            Storage::disk('wsdl_disk')->put('userBeesmart.xml', $wsdl);

            $wsdl = file_get_contents('http://' . config('uniqcast.serverName') . '/api/cwsNew/pruchaseBeesmart/server?wsdl');
            Storage::disk('wsdl_disk')->put('pruchaseBeesmart.xml', $wsdl);

            $this->info('WSDL files are created');
        } else {
            $this->error('Something went wrong! Missing Server Name or IP address');
            $this->error('You need to put Server Name or IP address in config file bee-provisioning-proxy/config/uniqcast.php attribute serverName');
        }
    }
}
