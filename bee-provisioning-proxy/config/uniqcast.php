<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Auth
    |--------------------------------------------------------------------------
    |
    | Username and Password for REST API
    |
    */

    'auth' => [
        'username' => 'uniqadmin',
        'password' => 'b1xQNFBzIKU'
    ],

    /*
    |--------------------------------------------------------------------------
    | URL
    |--------------------------------------------------------------------------
    |
    | Base URL for REST API
    |
    */
    'url' => 'https://demo.uniqcast.com:1443',


    /*
    |--------------------------------------------------------------------------
    | Server name
    |--------------------------------------------------------------------------
    |
    | set server name of IP addrese , is using for creating WSDL file on disk
    |
    */
    'serverName' => 'homestead.test',


    /*
    |--------------------------------------------------------------------------
    | Beesmart
    |--------------------------------------------------------------------------
    |
    | All beesmart params for WSDL
    |
    */

    'beesmart' => [
        'is_enabled' => true,
        'url' => 'https://62.138.6.28:20443',

        'target' => [
            'url' => 'http://62.138.6.28:20080',
        ],
        'trace' => true,
        'user' => '/OB/user/endpoint',
        'purchase' => '/OB/purchase/endpoint',
        'content' => '/OB/content/endpoint',

        'ssl' => [
            'is_enabled' => true,
            'stream_context' => stream_context_create([
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                ],
            ])
        ],

        'disk_path_to_wsdl' => storage_path('app/public/wsdl'),

        // 'trace' => true,
        // 'user' => '/OB/user/endpoint',
        // 'purchase' => '/OB/purchase/endpoint',
        // 'content' => '/OB/content/endpoint'
    ]


];
