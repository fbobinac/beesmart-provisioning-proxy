<?php

return [

    // Service configurations.

    'services'          => [

        'userBeesmart'              => [
            'name'              => 'UserBeesmartService',
            'class'             => 'App\Services\BeesmartService',
            'exceptions'        => [
                'Exception'
            ],
            'types'             => [
                'keyValue'              => 'Viewflex\Zoap\Demo\Types\KeyValue',
                'product'               => 'Viewflex\Zoap\Demo\Types\Product',
                'createSubscriber'      => 'App\Models\Beesmart\User\CreateSubscriber',
                'GetProfile'            => 'App\Models\Beesmart\User\GetProfile',
                'ProfileUpdate'         => 'App\Models\Beesmart\User\ProfileUpdate'
            ],
            'strategy'          => 'ArrayOfTypeComplex',
            'headers'           => [
                'Cache-Control'     => 'no-cache, no-store'
            ],
            'options'           => []
        ],
        'pruchaseBeesmart'              => [
            'name'              => 'PurchaseBeesmartService',
            'class'             => 'App\Services\PurchaseBeesmartService',
            'exceptions'        => [
                'Exception'
            ],
            'types'             => [
                'keyValue'          => 'Viewflex\Zoap\Demo\Types\KeyValue',
                'product'           => 'Viewflex\Zoap\Demo\Types\Product'
            ],
            'strategy'          => 'ArrayOfTypeComplex',
            'headers'           => [
                'Cache-Control'     => 'no-cache, no-store'
            ],
            'options'           => []
        ]

    ],


    // Log exception trace stack?

    'logging'       => true,




];
