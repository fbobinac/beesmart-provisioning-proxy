
## Installing

First need to pull app

```
git clone git@bitbucket.org:beemigrate/bee-provisioning-proxy.git
```

after pulling app you need to install all vendor component
```
cd bee-provisioning-proxy
composer install
```

after is needed to set some privileges
```
chmod -R 777 bootstrap/cache
chmod -R 777 storage
```

### Application Key
The next thing you should do after installing Laravel is set your application key to a random string. If you installed Laravel via Composer or the Laravel installer, this key has already been set for you by the php artisan key:generate command.

```
php artisan key:generate
```

at the end is needed create env file 
```
cp .env-example .env
```

## WSDL URL

### If you don't use Docker for environment
User
 - http://homestead.test/api/cws/userBeesmart/server?wsdl

Purchase Groupe
 - http://homestead.test/api/cws/pruchaseBeesmart/server?wsdl

### and for docker 

User
 - http://homestead.test/api/cwsNew/userBeesmart/server?wsdl

Purchase Groupe
 - http://homestead.test/api/cwsNew/pruchaseBeesmart/server?wsdl



## Config File Uniqcast
in config file are couple things<br />

in array beesmart attribute.is_enabled = true is used when you need just passthrough requesto to Beesmart WSDL service<br />
in array beesmart ssl.is_enabled = trueis used when you need when end point is https<br />
in array beesmart target.url is used when when end point for send request is different from getting XML WSDL <br />
in array beesmart trace is used to show more logs in soap calls<br />



## Create WSDL file in disk
for creating file on disk you need to use command , but beafore you need to set DNS name of IP address in config file 
(example in my case I set in /etc/host  192.168.10.10  homestead.test and set serverName = homestead.test)

serverName = ''

```
php artisan wsdl:save
```

file are created in bee-provisioning-proxy/storage/app/public/wsdl/ path 
this path is configurable in config bee-provisioning-proxy/config/uniqcast.php  beesmart.disk_path_to_wsdl


## Start Docker 

docker-compose up -d nginx postgres pgadmin workspace

## Enter in Docker Container

docker-compose exec --user=laradock workspace bash


## List current running Containers
docker ps

docker-compose ps


## Close all running Containers
docker-compose stop

## To stop single container do:

docker-compose stop {container-name}

### rebuild 

docker-compose build --no-cache workspace

### PostgreSQL

Use PgAdmin#
1 -- Run the pgAdmin Container (pgadmin) with the docker-compose up command. 
Example:
docker-compose up -d postgres pgadmin  <br />
2 - Open your browser and visit the localhost on port 5050: http://localhost:5050  <br />
3 - At login page use default credentials:  <br />
Username : pgadmin4@pgadmin.org  <br />
Password : admin  <br />



### POSTGRES Conf

POSTGRES_HOST=postgres  <br />
POSTGRES_DB=default  <br />
POSTGRES_USER=default  <br />
POSTGRES_PASSWORD=secret  <br />
POSTGRES_PORT=5432  <br />



## Postman Collection Test

To run test in Postman you need do next
 - Open Postman app
 - Click on import 
  -- and import files from bee-provisioning-proxy/tests/PostmanTest/Beesmart.postman_collection.json and Uniqcast.postman_collection.json
 - before run test need to insert some params in environment for that collection
 -- domain : dns name or IP address
 -- wsdl_path : path after domain
 -- device_uid : find available device from platform
 -- subscriber_uid : this part is defined in script in postman
 -- package_name : find package name from platform

bee-provisioning-proxy/tests/PostmanTest/Beesmart.postman_test_run.json and bee-provisioning-proxy/tests/PostmanTest/Uniqcast.postman_test_run.json are results of runed test with Postman App

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost you and your team's skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- [UserInsights](https://userinsights.com)
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)
- [Invoice Ninja](https://www.invoiceninja.com)
- [iMi digital](https://www.imi-digital.de/)
- [Earthlink](https://www.earthlink.ro/)
- [Steadfast Collective](https://steadfastcollective.com/)
- [We Are The Robots Inc.](https://watr.mx/)
- [Understand.io](https://www.understand.io/)
- [Abdel Elrafa](https://abdelelrafa.com)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).